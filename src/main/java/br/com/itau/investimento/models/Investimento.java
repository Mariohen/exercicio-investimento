package br.com.itau.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "O nome do investimento não pode ser nulo")
    @NotBlank(message = "O nome do investimento não pode estar em branco")
    @Column(unique = true)
    private String nomeInvestimento;

    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    @DecimalMin("00.10")
    private double rendimentoAoMes;

    public Investimento() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

}
