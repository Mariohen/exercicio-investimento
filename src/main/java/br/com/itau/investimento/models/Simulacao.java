package br.com.itau.investimento.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "O nome do cliente não pode ser nulo")
    @NotBlank(message = "O nome do cliente não pode estar em branco")
    private String nomeInteressado;

    @Email
    private String email;

    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    @DecimalMin("50.00")
    private double valorAplicado;

    @DecimalMin("1")
    private int quantidadeMeses;

    @NotNull (message = "Investimento deve ser informado")
    private int idInvestimento;


    //Contrutor vazio
    public Simulacao() {}

    //Getters e Setters


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }


    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }
}
