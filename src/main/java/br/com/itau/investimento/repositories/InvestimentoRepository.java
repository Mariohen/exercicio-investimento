package br.com.itau.investimento.repositories;

import br.com.itau.investimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository <Investimento, Integer> {

//    Investimento findFirstById(Integer id);

    Investimento findFirstById(Integer id);

}
