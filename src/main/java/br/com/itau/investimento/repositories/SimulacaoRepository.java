package br.com.itau.investimento.repositories;

import br.com.itau.investimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository <Simulacao, Integer> {
}
