package br.com.itau.investimento.controller;

import br.com.itau.investimento.DTOs.SaidaSimulacaoDTO;
import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.services.InvestimentoService;
import br.com.itau.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/Investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;
//*****************************************************************************************************************
//*****************************************************************************************************************
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento cadastrarInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento objetoInvestimento = investimentoService.salvarInvestimento(investimento);
        return objetoInvestimento;
    }
//*****************************************************************************************************************
//*****************************************************************************************************************
    @GetMapping
    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentoService.lerTodosOsInvestimentos();
    }
//*****************************************************************************************************************
//*****************************************************************************************************************
//*****************************************************************************************************************
//*****************************************************************************************************************
    @PostMapping("/{idInvestimento}/Simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaSimulacaoDTO cadastrarSimulacao(@PathVariable(name = "idInvestimento") int idInvestimento, @RequestBody @Valid Simulacao simulacao){
          System.out.println("Teste:  " + idInvestimento);
          SaidaSimulacaoDTO saidaSimulacaoDTO = simulacaoService.salvarSimulacao(simulacao, idInvestimento);
          return saidaSimulacaoDTO;
    }





}
