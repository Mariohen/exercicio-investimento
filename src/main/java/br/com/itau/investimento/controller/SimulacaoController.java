package br.com.itau.investimento.controller;

import br.com.itau.investimento.DTOs.SaidaListaSimulacaoDTO;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/Simulacoes")
public class SimulacaoController {


    @Autowired
    private SimulacaoService simulacaoService;


    @GetMapping
    public List<SaidaListaSimulacaoDTO> buscarTodas() {
        return simulacaoService.buscarTodasAsSimulacoes();
    }



}
