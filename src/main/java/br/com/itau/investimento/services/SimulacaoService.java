package br.com.itau.investimento.services;

import br.com.itau.investimento.DTOs.SaidaListaSimulacaoDTO;
import br.com.itau.investimento.DTOs.SaidaSimulacaoDTO;
import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.InvestimentoRepository;
import br.com.itau.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;


    public SaidaSimulacaoDTO salvarSimulacao(Simulacao simulacao, int idInvestimento) {
        Optional<Investimento> investimento = investimentoRepository.findById(idInvestimento);

        if (investimento.isPresent()) {
            investimentoRepository.findFirstById(idInvestimento);
            simulacao.setIdInvestimento(idInvestimento);
            Simulacao objetoSimulacao = simulacaoRepository.save(simulacao);
            SaidaSimulacaoDTO saidaSimulacaoDTO = new SaidaSimulacaoDTO();

            double total = 0;
            double valorMontante = 0;
            double juros = investimento.get().getRendimentoAoMes()/100;

            for (int i=0; i < simulacao.getQuantidadeMeses(); i++){
                total = simulacao.getValorAplicado()*juros;
                valorMontante = valorMontante + total;
            }

            saidaSimulacaoDTO.setMontante(simulacao.getValorAplicado() + valorMontante);
            saidaSimulacaoDTO.setRedimentoPorMes(investimento.get().getRendimentoAoMes());

            return saidaSimulacaoDTO;


        } else {
            throw new RuntimeException("Não existe o investimento informado");
        }
    }

    //retornar lista de simulacoes com um DTO
    public List<SaidaListaSimulacaoDTO> buscarTodasAsSimulacoes() {
        List<SaidaListaSimulacaoDTO> listaSimulacaoDTOS = new ArrayList<>();

        for (Simulacao simulacao : simulacaoRepository.findAll()) {
            SaidaListaSimulacaoDTO saidaListaSimulacaoDTO = new SaidaListaSimulacaoDTO();
            saidaListaSimulacaoDTO.setNomeInteressado(simulacao.getNomeInteressado());
            saidaListaSimulacaoDTO.setEmail(simulacao.getEmail());
            saidaListaSimulacaoDTO.setValorAplicado(simulacao.getValorAplicado());
            saidaListaSimulacaoDTO.setQuantidadeMeses(simulacao.getQuantidadeMeses());
            saidaListaSimulacaoDTO.setIdInvestimento(simulacao.getIdInvestimento());
            listaSimulacaoDTOS.add(saidaListaSimulacaoDTO);
     }

        return listaSimulacaoDTOS;
    }

}
