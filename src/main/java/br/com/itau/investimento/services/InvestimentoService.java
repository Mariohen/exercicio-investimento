package br.com.itau.investimento.services;

import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;


    public Investimento salvarInvestimento(Investimento investimento){
        Investimento objetoInvestimento = investimentoRepository.save(investimento);
        return objetoInvestimento;
    }

    //retorno de todos os registros
    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentoRepository.findAll();
    }

    //Buscar Investimento por ID
    public Investimento buscarInvestimentoPeloId(int id){
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);

        if(investimentoOptional.isPresent()){
            Investimento investimento = investimentoOptional.get();
            return investimento;
        }else {
            throw new RuntimeException("Investimento não foi encontrado");
        }
    }


}
