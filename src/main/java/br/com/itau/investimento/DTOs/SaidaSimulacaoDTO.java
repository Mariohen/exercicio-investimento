package br.com.itau.investimento.DTOs;

import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.models.Simulacao;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.*;
import java.util.List;

public class SaidaSimulacaoDTO {

    private double redimentoPorMes;
    private Double montante;

    public SaidaSimulacaoDTO() {}

    public double getRedimentoPorMes() {
        return redimentoPorMes;
    }

    public void setRedimentoPorMes(double redimentoPorMes) {
        this.redimentoPorMes = redimentoPorMes;
    }

    public Double getMontante() {
        return montante;
    }

    public void setMontante(Double montante) {
        this.montante = montante;
    }


}


