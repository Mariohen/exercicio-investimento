package br.com.itau.investimento.services;

import br.com.itau.investimento.DTOs.SaidaListaSimulacaoDTO;
import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.InvestimentoRepository;
import br.com.itau.investimento.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class SimulacaoServicesTeste {

    @MockBean
    private Simulacao simulacao;

    @MockBean
    private Investimento investimento;

    @MockBean
    private SimulacaoRepository simulacaoRepository;

    @MockBean
    private InvestimentoRepository investimentoRepository;


    SimulacaoService simulacaoService;

    @Test
    public void testarSalvarSimulacao (){
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).then(objeto -> objeto.getArgument(0));
    }

//    @Test
//    public void testarSalvarSimulacaoComInvestimentoQueNaoExiste (){
//        Optional<Simulacao> simulacaoOptional = Optional.of(simulacao);
//        Optional<Investimento> investimentoOptional = Optional.of(investimento);
//        Mockito.when(investimentoRepository.findById(99)).thenReturn(investimentoOptional);
//        Assertions.assertThrows(RuntimeException.class, () -> {
//            investimentoRepository.findById(99);});
//    }

    @Test
    public void testarLerTodasAsSimulacoes(){

        Simulacao simulacao1 = new Simulacao();
        simulacao1.setId(1);
        simulacao1.setNomeInteressado("Xablau de Paula");
        simulacao1.setEmail("xablaudepaula@xablau.com");
        simulacao1.setValorAplicado(5000.0);
        simulacao1.setQuantidadeMeses(36);
        simulacao1.setIdInvestimento(1);

        Simulacao simulacao2 = new Simulacao();
        simulacao2.setId(2);
        simulacao2.setNomeInteressado("Jose Xablau");
        simulacao2.setEmail("zechablaou@zeemail.com");
        simulacao2.setValorAplicado(7000.0);
        simulacao2.setQuantidadeMeses(12);
        simulacao2.setIdInvestimento(3);

        List<Simulacao> simulacoes = new ArrayList<>();
        simulacoes.add(simulacao1);
        simulacoes.add(simulacao2);

        Mockito.when(simulacaoRepository.findAll()).thenReturn(simulacoes);

        List<SaidaListaSimulacaoDTO> simulacoesEncontradas = simulacaoService.buscarTodasAsSimulacoes();

        Assertions.assertEquals(simulacoes.size(), simulacoesEncontradas.size());
        Assertions.assertEquals(simulacoes.get(0).getNomeInteressado(), simulacoesEncontradas.get(0).getNomeInteressado());
        Assertions.assertEquals(simulacoes.get(0).getIdInvestimento(), simulacoesEncontradas.get(0).getIdInvestimento());
        Assertions.assertEquals(simulacoes.get(1).getNomeInteressado(), simulacoesEncontradas.get(1).getNomeInteressado());
        Assertions.assertEquals(simulacoes.get(1).getIdInvestimento(), simulacoesEncontradas.get(0).getIdInvestimento());;

    }
}
