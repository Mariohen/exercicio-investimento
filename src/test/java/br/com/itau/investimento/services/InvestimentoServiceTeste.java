package br.com.itau.investimento.services;

import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.repositories.InvestimentoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class InvestimentoServiceTeste {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    Investimento investimento;

    @BeforeEach
    public void setUp(){
        this.investimento = new Investimento();
        investimento.setId(1);
        investimento.setNomeInvestimento("Rinha de galo");
        investimento.setRendimentoAoMes(7.00);
    }

    @Test
    public void testarSalvarInvestimento(){
        Mockito.when(investimentoService.salvarInvestimento(investimento)).then(objeto -> objeto.getArgument(0));
    }

    @Test
    public void testarLerTodosOsInvestimentos(){
       // List<InvestimentoService> investimentoServices = new ArrayList<>();
        Iterable<InvestimentoService> investimentoServices = new ArrayList<>();
        Mockito.when(investimentoRepository.findAll()).thenReturn(Mockito.anyIterable());
    }
}
