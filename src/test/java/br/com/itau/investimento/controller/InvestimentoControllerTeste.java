package br.com.itau.investimento.controller;

import br.com.itau.investimento.DTOs.SaidaSimulacaoDTO;
import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.services.InvestimentoService;
import br.com.itau.investimento.services.SimulacaoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTeste {

    @MockBean
    private InvestimentoService investimentoService;

    @MockBean
    private SimulacaoService simulacaoService;
//
//    @MockBean
//    private SaidaSimulacaoDTO saidaSimulacaoDTO;
//
//    Simulacao simulacao;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCadastrarInvestimento() throws Exception {
        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNomeInvestimento("Previdencia PGBL");
        investimento.setRendimentoAoMes(1.50);

        Mockito.when(investimentoService.salvarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        ObjectMapper objectMapper = new ObjectMapper();

        String produtoJson = objectMapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/Investimentos")
                .content(produtoJson)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$", CoreMatchers.containsString("Previdencia PGBL")));

        Mockito.verify(investimentoService, Mockito.times(1)).salvarInvestimento(Mockito.any(Investimento.class));
    }

    @Test
    public void testarLerTodosOsInvestimentos() throws Exception {

        List<Investimento> investimentos = new ArrayList<>();
        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNomeInvestimento("Previdencia PGBL");
        investimento.setRendimentoAoMes(1.50);
        investimentos.add(investimento);

        Mockito.when(investimentoService.lerTodosOsInvestimentos()).thenReturn(investimentos);

        mockMvc.perform(MockMvcRequestBuilders.get("/Investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarSimulacao() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setId(1);
        simulacao.setNomeInteressado("Testeeeee");
        simulacao.setQuantidadeMeses(12);
        simulacao.setEmail("teste@teste.com");
        simulacao.setValorAplicado(5000.00);
        simulacao.setIdInvestimento(1);

        SaidaSimulacaoDTO saidaSimulacaoDTO = new SaidaSimulacaoDTO();
        saidaSimulacaoDTO.setMontante(7000.00);
        saidaSimulacaoDTO.setRedimentoPorMes(1.00);

        Mockito.when(simulacaoService.salvarSimulacao(simulacao, 1)).thenReturn(saidaSimulacaoDTO);
        ObjectMapper objectMapper = new ObjectMapper();

        String leadJson = objectMapper.writeValueAsString(simulacaoService);

        mockMvc.perform(MockMvcRequestBuilders.post("/Investimentos/1/Simulacao")
                .content(leadJson)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$", CoreMatchers.equalTo(7000.00)));
    }



}


//
//
//
//
//
//    @Test
//    public void testarPesquisarPorId() throws Exception {
//        Produto produto = new Produto();
//        produto.setId(1);
//        produto.setNome("Produto 1");
//        produto.setDescricao("Descricao produto 1");
//        produto.setPreco(20.00f);
//
//        Mockito.when(produtoServices.buscarProdutoPeloId(1)).thenReturn(produto);
//
//        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers
//                        .jsonPath("$.id", CoreMatchers.equalTo(1)));
//    }
//

