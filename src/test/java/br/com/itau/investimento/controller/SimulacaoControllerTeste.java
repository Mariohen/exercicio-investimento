package br.com.itau.investimento.controller;

import br.com.itau.investimento.DTOs.SaidaListaSimulacaoDTO;
import br.com.itau.investimento.services.SimulacaoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTeste {

    @MockBean
    private SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testeBuscarTodasAsSimulacoes () throws Exception {
        SaidaListaSimulacaoDTO saidaListaSimulacaoDTO = new SaidaListaSimulacaoDTO();
        saidaListaSimulacaoDTO.setNomeInteressado("Mario");
        saidaListaSimulacaoDTO.setEmail("mario@email.com");
        saidaListaSimulacaoDTO.setValorAplicado(4000.00);
        saidaListaSimulacaoDTO.setQuantidadeMeses(12);
        saidaListaSimulacaoDTO.setIdInvestimento(1);

        SaidaListaSimulacaoDTO saidaListaSimulacaoDTO1 = new SaidaListaSimulacaoDTO();
        saidaListaSimulacaoDTO1.setNomeInteressado("Henrique");
        saidaListaSimulacaoDTO1.setEmail("henrique@email.com");
        saidaListaSimulacaoDTO1.setValorAplicado(4600.00);
        saidaListaSimulacaoDTO1.setQuantidadeMeses(36);
        saidaListaSimulacaoDTO1.setIdInvestimento(2);

        List<SaidaListaSimulacaoDTO> simulacoes = new ArrayList<>();
        simulacoes.add(saidaListaSimulacaoDTO);
        simulacoes.add(saidaListaSimulacaoDTO1);

        Mockito.when(simulacaoService.buscarTodasAsSimulacoes()).thenReturn(simulacoes);
        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.get("/Simulacoes")
                .content(mapper.writeValueAsString(simulacoes))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}

